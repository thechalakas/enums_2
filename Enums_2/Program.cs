﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enums_2
{
    //I have defined the enum here
    //it will start with the first element and sequentially assigne values starting from 0
    //look at the console output to see what happens with it


    enum Gender_1
    {
        Male, Female
    }

    enum Gender_2
    {
        Male = 5, Female = 20
    }

    [Flags]
    enum Gender_3
    {
        Male = , Female
    }

    class Program
    {
        static void Main(string[] args)
        {
           
            //I am casting the enum values to int because I want to access their underlying values

            Console.WriteLine(" Male enum is " + (int)Gender_1.Male);
            Console.WriteLine(" Female enum is " + (int)Gender_1.Female);

            //here is what I get when I dont cast

            Console.WriteLine(" Male enum is " + Gender_1.Male);
            Console.WriteLine(" Female enum is " + Gender_1.Female);

            //and here is where I might use a enum type

            Gender_1 boy = new Gender_1();
            Gender_1 girl = new Gender_1();

            boy = Gender_1.Male | Gender_1.Female;
            girl = Gender_1.Female;

            Console.WriteLine("boy is " + boy + "girl is " + girl);

            //here is an enum with the initial values modified.  look at the enum definition 
            //to see the connection

            //I am casting the enum values to int because I want to access their underlying values

            Console.WriteLine(" Male enum is " + (int)Gender_2.Male);
            Console.WriteLine(" Female enum is " + (int)Gender_2.Female);

            //here is what I get when I dont cast

            Console.WriteLine(" Male enum is " + Gender_2.Male);
            Console.WriteLine(" Female enum is " + Gender_2.Female);

            Console.ReadLine();


        }
    }
}
